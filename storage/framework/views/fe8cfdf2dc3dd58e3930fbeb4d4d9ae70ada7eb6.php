<?php $__env->startSection('css'); ?>
<style>
.form-inline
{
    margin-bottom:20px;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
Articles
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

<form class="form-inline" method="get" action="<?php echo e(url('articles/search')); ?>">
    <div class="form-group">
        <label for="q" class="sr-only">Search</label>
        <input type="text" name="q" id="q" class="form-control" placeholder="Search">
    </div>
        <input type="submit" value="submit" class="btn btn-info">
</form>


    <?php foreach($articles as $article): ?>

        <div class="perarticle">
            <div id="col1">
                    <?php echo e($article->title); ?>

                    <p id="date"><?php echo e($article->created_at); ?></p>
            </div>

            <div id="col2">

                <a href="">EDIT</a> <span>|</span>
                <a href="">DELETE</a>

            </div>

        </div>

    <?php endforeach; ?>

    <a href="<?php echo e(url('articles/create')); ?>"><button class="btn btn-info">Create New Article</button></a>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">

$(function(){
    $("#q").keyup(function(){
    $("#q").autocomplete({
    source:"<?php echo e(URL('/articles/autocomplete')); ?>",
    minLength: 2,

    });
    $("#q").autocomplete("widget").css('background-color', '#eeeeee');
    });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>