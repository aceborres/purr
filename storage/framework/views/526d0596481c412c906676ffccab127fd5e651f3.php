<?php $__env->startSection('css'); ?>
<style>
h3
{
    color:red;
}
.button
{
    border:2px solid gray;
    padding:8px;
    background:none;
    font-size:12px;
    color:black;
}
.button:hover
{
    border:2px solid #bebebe;
}
.perarticle
{
    border:1px solid #ccc;
    margin-bottom:10px;
    /*box-shadow: 0px 0px 5px #61C5FA;*/
    border-radius:5px;
    padding:5px;
    height:50px;
}
.perarticle:hover
{
    border-color: #9ecaed;
    box-shadow: 0 0 10px #9ecaed;
}
#date
{
    font-size:12px;
    font-style:italic;
    color:gray;
}
#col1
{
    position:absolute;
    margin-left:30px;
}
#col2
{
    margin-top:10px;
    float:right;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
Articles
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>


        <div class="perarticle">
            <?php echo $__env->yieldContent('articles'); ?>
        </div>



    <a href="<?php echo e(url('articles/create')); ?>"><button class="button">Create New Article</button></a>
    <a href="<?php echo e(url('articles/manage')); ?>"><button class="button">Manage Articles</button></a>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('app2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>