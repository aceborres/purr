<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Main Page</title>
        <link rel="stylesheet" href="<?php echo e(asset(app.css)); ?>" media="screen" title="no title" charset="utf-8">
    </head>
    <body>
        <div class="container">
                    <?php echo $__env->yieldContent('content'); ?>;
        </div>
    </body>
</html>
