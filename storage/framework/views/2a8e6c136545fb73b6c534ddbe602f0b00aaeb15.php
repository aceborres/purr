<?php $__env->startSection('title'); ?>
Articles
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

<?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php foreach($articles as $article): ?>

        <div class="perarticle">
            <div id="col1">
                    <?php echo e($article->title); ?>

                    <p id="date"><?php echo e($article->created_at); ?></p>
            </div>

            <div id="col2">

                <a href="<?php echo e(url('/articles/' . $article->id)); ?>">Read More...</a>

            </div>

        </div>

    <?php endforeach; ?>
        <div class="pull-left">
            <a href="<?php echo e(url('articles/create')); ?>"><button class="btn btn-info">Create Own Purr</button></a>
            <a href="<?php echo e(url('articles/manage')); ?>"><button class="btn btn-primary">Manage Your Purr</button></a>
        </div>

        <div class="pull-right">
            <?php echo $articles->render(); ?> <!-- pagination -->
        </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

<script>
$('document').ready(function() {
    setTimeout(function() {
        $('#flash').slideUp();
    }, 3000);
});
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('app2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>