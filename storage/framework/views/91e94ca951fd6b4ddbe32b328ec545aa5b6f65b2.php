<?php $__env->startSection('css'); ?>
<!-- Latest compiled and minified CSS -->
<style>
.body
{
    max-width: 100%;
}
.alert
{
    padding:5px 10px 5px 30px;
    width:400px;
}
</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1>Create New Purr</h1>

    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <form action="<?php echo e(url('articles')); ?>" method="POST">

        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" placeholder="Title" class="form-control">
        </div>

        <div class="form-group">
            <label for="Body">Body</label>
            <textarea name="body" rows="8" cols="40" class="form-control body" placeholder="Body"></textarea>
        </div>

        <input type="submit" class="btn btn-info" value="Submit">

    </form>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
})
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>