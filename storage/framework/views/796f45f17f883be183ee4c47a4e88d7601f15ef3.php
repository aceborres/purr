<?php $__env->startSection('css'); ?>
<style>
#goback
{
    margin-top:100px;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1><?php echo e($articles->title); ?></h1>

    <?php echo e($articles->body); ?>


    <div class="push-left" style="bottom:40px;position:absolute">
        <a class="btn btn-link" href="<?php echo e(url('articles')); ?>" style="text-decoration:none">
            <span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>
            Go Back </a>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('app2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>