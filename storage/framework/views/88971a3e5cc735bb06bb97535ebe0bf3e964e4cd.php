<?php if($errors->any()): ?>
        <ul class="alert alert-danger">
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
<?php endif; ?>

<?php if(Session::has('message')): ?>
    <div class="alert alert-success" id="flash">
        <span class="glyphicon glyphicon-ok"></span>
        <em> <?php echo session('message'); ?></em>
    </div>
<?php endif; ?>
