@extends ('app2')

@section('title')
Manage your Purr
@stop


@section('content')
@include('alerts.alert')

<table class="table table-hover">
    <thead>
      <tr class="info">
        <th>Title</th>
        <th>Date</th>
        <th class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($ownArticles as $ownArticle)
        <tr>
            <td>{{ $ownArticle->title }}</td>
            <td id="date">{{ $ownArticle->created_at }}</td>
            <td class="text-center">
                <button class="btn btn-primary btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="left" title="Edit"><span class="glyphicon glyphicon-edit glyphicon"></span></button>
                <button class="btn btn-primary btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="right" title="Delete"><span class="glyphicon glyphicon-remove"></span></button>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>

@stop

@section('javascript')

<script>
$('document').ready(function() {
    setTimeout(function() {
        $('#flash').slideUp();
    }, 3000);
});

</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

@endsection
