@extends ('app2')

@section('title')
Purrfile
@stop


@section('content')
<h3>{{ $profile->username }}</h3>
<div class="pull-left">
    <a href="{{ route('purrfile.{purrfile}.articles.index', [$profile->username]) }}">
</div>
@include('alerts.alert')


@stop

@section('javascript')

<script>
$('document').ready(function() {
    setTimeout(function() {
        $('#flash').slideUp();
    }, 3000);
});
</script>

@endsection
