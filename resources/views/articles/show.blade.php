@extends('app2')

@section('css')
<style>
#goback
{
    margin-top:100px;
}
</style>
@stop

@section('content')

    <h1>{{ $articles->title}}</h1>

    {{ $articles->body }}

    <div class="push-left" style="bottom:40px;position:absolute">
        <a class="btn btn-link" href="{{ url('articles') }}" style="text-decoration:none">
            <span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>
            Go Back </a>
    </div>


@stop
