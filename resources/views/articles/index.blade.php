@extends ('app2')

@section('title')
Articles
@stop


@section('content')

@include('alerts.alert')

    @foreach($articles as $article)

        <div class="perarticle">
            <div id="col1">
                    {{ $article->title }}
                    <p id="date">{{ $article->created_at}}</p>
            </div>

            <div id="col2">

                <a href="{{url('/articles/' . $article->id)}}">Read More...</a>

            </div>

        </div>

    @endforeach
        <div class="pull-left">
            <a href="{{url('articles/create')}}"><button class="btn btn-info">Create Own Purr</button></a>
            <a href="{{url('articles/manage')}}"><button class="btn btn-primary">Manage Your Purr</button></a>
        </div>

        <div class="pull-right">
            {!! $articles->render() !!} <!-- pagination -->
        </div>

@stop

@section('javascript')

<script>
$('document').ready(function() {
    setTimeout(function() {
        $('#flash').slideUp();
    }, 3000);
});
</script>

@endsection
