@extends('app2')

@section('css')
<!-- Latest compiled and minified CSS -->
<style>
.body
{
    max-width: 100%;
}
.alert
{
    padding:5px 10px 5px 30px;
    width:400px;
}
</style>

@stop

@section('content')

    <h1>Create New Purr</h1>

    @include('alerts.alert')

    <form action="{{ url('articles') }}" method="POST">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" placeholder="Title" class="form-control">
        </div>

        <div class="form-group">
            <label for="Body">Body</label>
            <textarea name="body" rows="8" cols="40" class="form-control body" placeholder="Body"></textarea>
        </div>

        <input type="submit" class="btn btn-info" value="Submit">

    </form>

@stop

@section('javascript')
<script>
$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
})
</script>
@stop
