@extends('app2')

@section('css')
<!-- Latest compiled and minified CSS -->
<style>
.form-control
{
    width:400px;
}
.body
{
    width:400px;
    max-width:400px;
    min-width:400px;
}
.alert
{
    padding:5px 10px 5px 30px;
    width:400px;
}
</style>

@stop

@section('title')
Create New Article
@stop

@section('content')

<div class="container">

    @if($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    @endif

    <form action="{{'/articles'}}" method="POST">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" placeholder="Title" class="form-control">
        </div>

        <div class="form-group">
            <label for="Body">Body</label>
            <textarea name="body" rows="8" cols="40" class="form-control body" placeholder="Body"></textarea>
        </div>

        <input type="submit" class="btn btn-info" value="Submit">

    </form>

</div>

@stop
