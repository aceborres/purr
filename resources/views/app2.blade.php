<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Purrblog</title>
        <link rel="icon" href="{{ url('logo.png') }}">
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/dist/css/bootstrap.css') }}">
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{'/app.css'}}" media="screen" title="no title" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @yield('css')

        <style>

        body
        {
            font-family: Raleway;
        }

        .container
        {
            margin-bottom:30px;
        }

        .navigation
        {
            list-style:none;
            margin-top:50px;
            display: flex;           /* establish flex container */
            justify-content: center; /* center items vertically, in this case */
            align-items: center;
        }

        .navigation a
        {
            color:gray;
            text-decoration:none;
        }

        .navigation a:hover
        {
            color:black;
            text-decoration:none;
        }

        .navigation li
        {
            display:inline-block;
            margin-right:20px;
        }
        img {
            margin: 0px auto;
            display: block;
        }

        </style>
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid" id="navfluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigationbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" style="padding:5px" href="{{ url('articles') }}"><img src="{{ url('logo.png')}}" width="40px"></a>
                </div>
                <div class="collapse navbar-collapse" id="navigationbar">
                    <ul class="nav navbar-nav navbar-left" style="display: {{ (Auth::check())? '': 'none' }}">
                        <li class="active"><a href="{{url('articles')}}">Purrblog</a></li>
                        <li><a href="{{url('user')}}">Purrfile</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" style="display: {{ (Auth::check())? '': 'none' }}">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Change Password</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ url('auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                        <!-- <li><a href="{{ url('auth/logout') }}">Logout</a></li> -->
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container">
            @yield('content')
        </div>

        @yield('javascript')
        <script src="{{ asset('vendor/jquery/dist/jquery.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.js') }}"></script>
    </body>
</html>
