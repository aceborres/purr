@extends('app2')

@section('content')
<div class="container">
    <div class="row">
        <div class="" style="width:420px;margin:0px auto; margin-top: 20px;">
            <div class="panel panel-info">
                <div class="panel-heading">Login to Purr</div>
                <div class="panel-body">
                    <img src="{{ url('logo.png') }}" width="35%">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('auth/login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="ontrol-label sr-only">Username</label>

                            <div class="col-md-8 col-md-offset-2">
                                <input type="username" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="control-label sr-only">Password</label>

                            <div class="col-md-8 col-md-offset-2">
                                <input type="password" class="form-control" name="password" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-info col-md-12">
                                    Login <span class="glyphicon glyphicon-log-in"></span>
                                </button>
                            </div>
                            <a class="btn btn-link btn-sm" href="{{ url('/password/email/') }}">Forgot Your Password?</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="text-right">
                <a href="{{ url('/auth/register') }}"><i>Not Registered? Create an account</i></a>
            </div>
        </div>
    </div>
</div>
@endsection
