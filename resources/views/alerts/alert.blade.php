@if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
@endif

@if(Session::has('message'))
    <div class="alert alert-success" id="flash">
        <span class="glyphicon glyphicon-ok"></span>
        <em> {!! session('message') !!}</em>
    </div>
@endif
