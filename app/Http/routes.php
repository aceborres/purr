<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('auth/login');
});

Route::get('PageNotFound', function() {
    return view('alerts.PageNotFound'); //edited on Exceptions/Handler.php
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

//forgot password
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

//reset password
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Route::resource('/user', "UserController");

// Route::resource('user.about','UserController@about');
Route::group(array('middleware' => 'auth'), function()
{
    Route::get('about','UserController@about');
    Route::get('contact', 'UserController@contact');
    Route::get('/articles/autocomplete', 'SearchController@autocomplete');
    Route::resource('articles', 'ArticlesController');

});

Route::group(array('middleware' => 'checkAuth'), function()
{
    Route::get('auth/login','Auth\AuthController@getLogin');
});

Route::group(array('middleware' => 'checkProfileAuth'), function()
{
    Route::resource('articles/profile','PurrProfileController');
});
