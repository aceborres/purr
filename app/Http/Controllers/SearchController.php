<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Response;

class SearchController extends Controller
{
    public function autocomplete()
    {
        	$term = Input::get('term');

        	$results = array();

        	$queries = DB::table('articles')
        		->where('title', 'LIKE', '%'.$term.'%')
        		->take(5)->get();

        	foreach ($queries as $query)
        	{
        	    $results[] = ['value' => $query->title];
        	}
            return Response::json($results);
    }
}
