<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Article;

use App\User;

class ManageArticlesController extends Controller
{
    public function index($username) {

        if(\Auth::user()->username != $username) {
            return redirect('purrfile');
        }

        $checkUsername = User::where('username', $username)->first();
        if(!isset($checkUsername)){
            return view('alerts.PageNotFound');
        }
        $ownArticles = Article::where('username', $username)->get();
        return view('manage.index')->with([
            'username' => $username,
            'ownArticles' => $ownArticles,
        ]);
    }
}
