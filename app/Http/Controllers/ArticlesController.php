<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;

use App\Article;

use App\Http\Requests;

class ArticlesController extends Controller
{
    public function index()
    {
        //$articles = Article::all();
        $articles = Article::latest()->paginate(10);
        return view('articles.index', compact('articles'));
    }

    public function show($id)
    {
        try
        {
            $articles = Article::findorFail($id);
        }
        catch(ModelNotFoundException $e) {
            return view('alerts.PageNotFound');
        }
        return view('articles.show')->with('articles', $articles);
    }

    public function search()
    {
        $term = Input::get('q');

        $articles = Article::where('title', $term)->get();

        // echo $articles
        return view('articles/manage', compact('articles'));
    }

    public function create()
    {
        return view('articles.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:5',
            'body' => 'required|min:5',
        ]);

        $article = new Article;
        $article->title = $request->title;
        $article->body = $request->body;
        $article->save();
        // redirect
        $request->session()->flash('message', 'Article Successfully Created!');
        return redirect('articles');
    }

    public function edit(Request $request)
    {
        Article::create($request->all());
        return redirect('articles');
    }

    public function manage()
    {
        $articles = Article::latest()->get();
        return view('articles.manage', compact('articles'));
    }

}
