<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    public function index()
    {
        $first = 'Main';
        $last= 'Page';
        $title = [];
        $title['first'] = "Main";
        $title['last'] = "Page";
        // return view("test")->with(['first' => 'Main','last' => 'Page']); //array
        return view("test", compact('first','last')); // two or more variables, use compact
        //return view("test", $title); //using array
        //return view("test")->with('first', $first);
    }

    public function about()
    {
        return 'about us';
    }

    public function contact()
    {
        return view('contact');
    }
}
